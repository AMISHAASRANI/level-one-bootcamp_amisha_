//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>
int main()
{
    float h,d,b,volume;      
    printf("\nenter the values for h , d and b:");
    scanf("%f%f%f",&h,&d,&b);
    volume=(((h*d*b)+(d/b))/3);
    printf("with h=%.2f , b=%.2f ,d=%.2f units ,the volume of tromboloid is %.2f cubic unit\n",h,b,d,volume);
    return 0;
}
