#include <stdio.h>
float input()
{
    float a; 
    printf("Enter a number\n");
    scanf("%f",&a);
    return a;
}

float find_vol(float h,float b,float d)
{
    float volume;
    volume=(((h*d*b)+(d/b))/3);
    return volume;
}

void output(float h,float b,float d,float volume)
{
    printf("with h=%.2f b= %.2f d=%.2f units the volume of tromboloid is %.2f cubic unit\n",h,b,d,volume);
}

int main()
{
    float x,y,z,result;
    x=input();
    y=input();
    z=input();
    result=find_vol(x,y,z);
    output(x,y,z,result);
    return 0;
}